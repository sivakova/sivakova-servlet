function sendMessage(message){
jQuery.ajax({
      url: "/messages",
      type: "POST",
      dataType: "json", // expected format for response
      contentType: "application/json", // send as JSON
      data: message,
      success: function() {
        getNewMessage();
     },
    });
}

function updateMessagesList(newMessages) {
	var currentMessagesList = jQuery(".chat_messages_list");

	var currentMessages = "";
	newMessages.forEach(function (message) {
		currentMessages += message.login+ "[" + message.time + "]: " + message.message + "\n";
	});
	currentMessagesList.val(currentMessages);
}

function getNewMessage(){
    jQuery.get("/messages?date_time=" + formated_date, null, function(newMessages) {
		updateMessagesList(newMessages);
	});
}

jQuery(document).ready(function() {
	jQuery(".chat_submit_button").click(function() {
		sendMessage(jQuery(".chat_new_message").val());
		jQuery(".chat_new_message").val("");
	});

	jQuery(".chat_new_message").keyup(function(event){
        if(event.keyCode == 13){
            $(".chat_submit_button").click();
        }
    });

    jQuery(".chat_logout_button").click(function() {
        $.cookie('login', null);
        $.cookie('password', null);
        location.reload();
    });

    getNewMessage();
	setInterval(getNewMessage, 5000);
});