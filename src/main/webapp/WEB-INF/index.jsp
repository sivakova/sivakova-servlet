<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <script src="http://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="
		sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <link rel="stylesheet" href="main.css">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8">

    <script type="text/javascript" src="jquery.cookie.js"></script>
    <script src="main.js"></script>
</head>
<body>
<div class="container-fluid" style="height:100%">
    <h1>Чат</h1>

    <div class="row chat_messages_form" style="height: 75%; margin-bottom: 10px;">
        <div class="col-xs-14" style="height:100%">
            <textarea style="height: 100%; width: 100%;" class="chat_messages_list" readonly></textarea>
        </div>
        <div class="row">
            <div class="col-xs-10">
                <input type="text" style="width: 100%;" class="form-control chat_new_message" autofocus>
            </div>
            <div class="col-xs-2">
                <button type="button" class="btn btn-primary chat_submit_button">Отправить</button>
            </div>
            <div class="col-xs-2">
                <button type="button" class="btn btn-primary chat_logout_button">Выход</button>
            </div>

        </div>
    </div>
</div>
</body>
</html>