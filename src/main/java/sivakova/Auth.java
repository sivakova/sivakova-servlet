package sivakova;

import javax.servlet.http.Cookie;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;


public class Auth {
    public static boolean auth(String login, String password)
    {
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection("jdbc:postgresql://194.87.187.238/", "sivakova", "sivakova");
            PreparedStatement select = conn.prepareStatement("SELECT * FROM authServlet WHERE login = ? AND password = ?");
            select.setString(1, login);
            select.setString(2, password);
            ResultSet result = select.executeQuery();

            while (result.next()){
                return true;
            }

            try {
                conn.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;

    }

    public static boolean auth(Cookie[] cookies)
    {
        try {
            Map<String, Cookie> cookieMap = new HashMap<>();
            for (Cookie cookie : cookies) {
                cookieMap.put(cookie.getName(), cookie);
            }

            String login = cookieMap.get("login").getValue();
            String password = cookieMap.get("password").getValue();

            if(Auth.auth(login, password)) {
                return true;
            }
        } catch (NullPointerException e) {
            //не авторизован
            System.out.println("не авторизован");
            return false;
        }
        return false;
    }
}
