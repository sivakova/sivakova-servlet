package sivakova;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthorizationServlet extends HttpServlet{
    public AuthorizationServlet() {
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(Auth.auth(request.getCookies())) {
            response.sendRedirect("/");
            return;
        }
        request.getRequestDispatcher("/WEB-INF/auth.jsp").forward(request, response);
    }
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        if(Auth.auth(login, password)) {
            Cookie loginCookie = new Cookie("login", login);
            Cookie passwordCookie = new Cookie("password", password);
            loginCookie.setMaxAge(60*60*1);
            passwordCookie.setMaxAge(60*60*1);
            loginCookie.setPath("/");
            passwordCookie.setPath("/");
            response.addCookie(loginCookie);
            response.addCookie(passwordCookie);

            response.sendRedirect("/");
        } else {
            response.sendRedirect("/auth/");
        }

    }
}
