package sivakova;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.time.Instant;
import java.util.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class MyServlet extends HttpServlet {
    private Connection conn;
    private PreparedStatement insert;
    private PreparedStatement selectAll;
    private PreparedStatement selectLastThreeDays;

    public void init() {
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection("jdbc:postgresql://194.87.187.238/", "sivakova", "sivakova");
            insert = conn.prepareStatement("INSERT INTO messageServlet (login, time, message) VALUES" +
                    "(?, ?, ?)");
            selectAll = conn.prepareStatement("SELECT * FROM messageServlet");
            selectLastThreeDays = conn.prepareStatement("SELECT * FROM messageServlet WHERE time>=('2017-05-18 23:28:24.629')");
            System.out.println("connection done");
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("connection failed");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        if(!Auth.auth(request.getCookies())) {
            response.setStatus(401);
            return;
        }

        response.setHeader("Content-type", "application/json;charset=UTF-8");

        try {
            //ResultSet result = selectAll.executeQuery();
            ResultSet result = selectLastThreeDays.executeQuery();
            JSONArray resultJson = new JSONArray();
            while (result.next()){
                JSONObject messageObject = new JSONObject();
                messageObject.put("login", result.getString("login"));
                messageObject.put("time", result.getTimestamp("time").toString());
                messageObject.put("message", result.getString("message"));
                //                System.out.println(key + " : " + messages.get(key));
                resultJson.add(messageObject);
//                System.out.println(result.getString("login") + (result.getString("time")) + ": " + result.getString("message"));
            }
            response.getWriter().write(resultJson.toString());

            response.setStatus(200);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(!Auth.auth(request.getCookies())) {
            response.setStatus(401);
            return;
        }

        String message = new String();

        response.setHeader("Content-type", "application/json;charset=UTF-8");

        try {
            message = request.getReader().readLine();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (message.length() > 0) {

            Instant now = Instant.now();
            Timestamp current = Timestamp.from(now);
            JSONArray resultJson = new JSONArray();
            JSONObject messageObject = new JSONObject();

            messageObject.put("time", current);
            messageObject.put("message", message);
            resultJson.add(messageObject);

            System.out.println("Received message: '" + message + "'");

            Cookie[] cookies = request.getCookies();
            Map<String, Cookie> cookieMap = new HashMap<>();
            for (Cookie cookie : cookies) {
                cookieMap.put(cookie.getName(), cookie);
            }

            String login = cookieMap.get("login").getValue();

            try {
                insert.setString(1, login);
                insert.setTimestamp(2, current);
                insert.setString(3, message);
                insert.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            response.getWriter().write(resultJson.toString());
            response.setStatus(200);
        }
    }

    public void destroy(){
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
