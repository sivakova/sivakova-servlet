package sivakova;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class IndexServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(!Auth.auth(request.getCookies())) {
            response.sendRedirect("/auth");
            return;
        }
        request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);

    }
}
